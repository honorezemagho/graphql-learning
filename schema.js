const { gql } = require("apollo-server");

module.exports = gql`
  type Session {
    id: ID!
    title: String!
    description: String
    startsAt: String
    endsAt: String
    room: String
    day: String
    format: String
    track: String
    level: String
    favorite: Boolean
    speakers: [Speaker]
  }

  input SessionInput {
    id: ID
    title: String
    description: String
    startsAt: String
    endsAt: String
    room: Room
    day: String
    format: String
    track: String
    level: String
    favorite: Boolean
  }

  enum Room {
    EUROPA
    SOL
    SATURN
  }

  type BasicSession {
    id: ID
    name: String
  }

  input BasicSessionInput {
    id: ID
    name: String
  }

  type Speaker {
    id: ID!
    name: String
    bio: String
    sessions: [BasicSession]
  }

  input SpeakerInput {
    id: ID
    name: String
    bio: String
    sessions: [BasicSessionInput]
  }

  type Query {
    sessions(filter: SessionInput): [Session]
    sessionById(id: ID!): Session
    speakers: [Speaker]
    speakerById(id: ID!): Speaker
  }

  type Mutation {
    toggleFavoriteSession(id: ID!): Session
    addSession(input: SessionInput): Session
  }
`;
