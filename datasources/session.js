const sessions = require("../data/sessions.json");
const { DataSource } = require("apollo-datasource");
const _ = require("lodash");

class SessionAPI extends DataSource {
  constructor() {
    super();
  }

  getSessions(args) {
    return _.filter(sessions, args);
  }

  getSessionById(id) {
    return sessions.find((session) => Number(session.id) == id);
  }

  toggleFavoriteSession(id) {
    const session = this.getSessionById(id);
    session.favorite = !session.favorite;
    return session;
  }
  
  async addSession(input) {
    const session = {
      id: sessions.length + 1,
      ...input,
    };
    sessions.push(session);
    return session;
  }
}

module.exports = SessionAPI;
