const speakers = require("../data/sessions.json");

const { RESTDataSource } = require("apollo-datasource-rest");
const _ = require("lodash");

class SpeakerAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = `http://localhost:3000/speakers`;
  }

  async getSpeakers() {
    return this.get("/");
  }

  async getSpeakerById(id) {
    return this.get(`/${id}`);
  }
}

module.exports = SpeakerAPI;
