module.exports = {
  sessions: (_, args, { dataSources }) => {
    const filter = args.filter ?? {};
    return dataSources.sessionAPI.getSessions(filter);
  },
  sessionById(_, { id }, { dataSources }) {
    return dataSources.sessionAPI.getSessionById(id);
  },
  speakers: (_, __, { dataSources }) => {
    return dataSources.speakerAPI.getSpeakers();
  },
  speakerById(_, { id }, { dataSources }) {
    return dataSources.speakerAPI.getSpeakerById(id);
  },
};
