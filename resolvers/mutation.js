module.exports = {
        addSession: async (_, { input }, { dataSources }) => {
            return dataSources.sessionAPI.addSession(input);
        }, 

        toggleFavoriteSession(_, { id }, { dataSources }) {
            return dataSources.sessionAPI.toggleFavoriteSession(id);
        }
}