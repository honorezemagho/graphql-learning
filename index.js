const { ApolloServer, gql, ApolloError } = require("apollo-server");
const SessionAPI = require("./datasources/session");
const SpeakerAPI = require("./datasources/speakers");

const resolvers = require("./resolvers");

const typeDefs = require("./schema");

const dataSources = () => ({
  sessionAPI: new SessionAPI(),
  speakerAPI: new SpeakerAPI(),
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources,
  csrfPrevention: true,
  cache: "bounded",
  debug: false,
  formatError: (err) => {
    if(err.extensions.code == 'INTERNAL_SERVER_ERROR') {
      return new ApolloError("We are having some trouble", "ERR-UNAVAILABLE", {token : "unique token"})
      }
      return err;
  }
});

server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
  console.log(`The GraphQL server is listening on ${url}`);
});
